import java.io.IOException;

import com.oopscraft.gealerts.scraper.ImageDownloader;
import com.oopscraft.gealerts.scraper.Scraper;

public class MainClass {

	public static void main(String [] args) throws IOException, InterruptedException {
		if(args.length != 1) {
			System.out.println("First run this program with 'itemsToJSON' command, then run it with 'downloadImageData'");
			return;
		}
		String arg = args[0];
		if("itemsToJSON".equalsIgnoreCase(arg)) {
			System.out.println("Download all GE item info to JSON...");
			new Scraper().begin();
		}
		else if("downloadImageData".equalsIgnoreCase(arg)) {
			new ImageDownloader().begin("runescape_items.json");
		}
		else {
			System.out.println("First run this program with 'itemsToJSON' command, then run it with 'downloadImageData'");
		}
	}
}
