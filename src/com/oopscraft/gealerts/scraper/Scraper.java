package com.oopscraft.gealerts.scraper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * This class is responsible for scraping the Runescape to create a database of items. 
 * This is necessary because Runescape's API is bad at finding things by name. 
 * @author Computadora
 *
 */
public class Scraper {
	
	//Confirm 36 really the last valid category.
	private final int lastCategory = 36;
	
	private Pattern p;
	public Scraper() {
		String idRegex = ".*obj_sprite\\.gif\\?id=(\\d+)$";
		this.p = Pattern.compile(idRegex);
	}
	public void begin() throws IOException, InterruptedException {
		List<ScrapedItem> allItems = new ArrayList<ScrapedItem>();
		for(int category = 0;category <= this.lastCategory;category++) {
			System.out.println("Working on category:"+String.valueOf(category));
			allItems.addAll(this.scrapeCategory(category, 1));
		}
		
		System.out.println("About to write to json...");
		Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
		String database = gson.toJson(allItems);
		File output = new File("runescape_items.json");
		FileWriter writer = new FileWriter(output);
		writer.write(database);
		writer.flush();
		writer.close();
		System.out.println("Done!");
	}
	
	/**
	 * Collects data from startingPage to the last page for a category.
	 */
	private List<ScrapedItem> scrapeCategory(int category, int startingPage) throws IOException, InterruptedException {
		List<ScrapedItem> categoryItems = new ArrayList<ScrapedItem>();
		Document doc = Jsoup.connect(this.generateURL(category, startingPage)).get();
		categoryItems.addAll(this.scrapeTable(doc.body().getElementsByTag("table").get(0)));
		int nextPage = startingPage + 1;
		String nextPageButtonText = "paginationListItem" + String.valueOf(nextPage);
		if(doc.getElementById(nextPageButtonText) != null) {
			System.out.println("About to scrape page "+ String.valueOf(nextPage) +" of category " + String.valueOf(category));
			categoryItems.addAll(this.scrapeCategory(category, nextPage));
		}
		return categoryItems;
	}
	private String generateURL(int category, int page) {
		String retVal = "http://services.runescape.com/m=itemdb_rs/catalogue?cat=" + String.valueOf(category);
		if(page > 1) {
			retVal += "&page="+String.valueOf(page);
		}
		return retVal;
	}
	private List<ScrapedItem> scrapeTable(Element table) {
		List<ScrapedItem> tableItems = new ArrayList<ScrapedItem>();
		if(table == null) {
			return tableItems;
		}
		for(Element item : table.getAllElements()) {
			if("tr".equalsIgnoreCase(item.tagName())) {
				ScrapedItem scraped = this.scrapeRow(item);
				if(scraped == null) {
					continue;
				}
				tableItems.add(scraped);
			}
		}
		return tableItems;
	}
	private ScrapedItem scrapeRow(Element tableRow) {
		ScrapedItem item = new ScrapedItem();
		for(Element img : tableRow.getElementsByTag("img")) {
			String src = img.attr("src");
			if(src.contains("tick.png")) {
				item.setMembers(true);
			}
			else {
				Matcher m = p.matcher(src);
				if(!m.matches()) {
					continue;
				}
				item.setId(Long.valueOf(m.group(1)));
				item.setName(img.attr("title"));
				item.setImageUrl(src);
			}
		}
		if(item.getId() == null) {
			return null;
		}
		else {
			return item;
		}
	}

}
