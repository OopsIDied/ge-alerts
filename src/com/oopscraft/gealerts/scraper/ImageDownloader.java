package com.oopscraft.gealerts.scraper;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.nio.file.*;

public class ImageDownloader {

	public void begin(String jsonSource) throws IOException {
		Gson gson = new Gson();
		JsonReader reader = new JsonReader(new FileReader(jsonSource));
		ScrapedItem[] rsItems = gson.fromJson(reader, ScrapedItem[].class);
		for(int i = 0;i<rsItems.length;i++) {
			String imgURL = rsItems[i].getImageUrl();
			String path = "images/";
			path = path + String.valueOf(rsItems[i].getId())+".gif";
			try(InputStream in = new URL(imgURL).openStream()){
				if(i % 100 == 0) {
					double progress = (double)i / rsItems.length;
					System.out.println("Progress:"+String.valueOf(progress));
				}
				Files.copy(in, Paths.get(path));
			}
		}
		System.out.println("Done!");
	}
}
