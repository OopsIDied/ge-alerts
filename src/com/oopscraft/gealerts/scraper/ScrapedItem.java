package com.oopscraft.gealerts.scraper;

public class ScrapedItem {
	
	private String name;
	private String imageUrl;
	private Long id;
	private boolean members;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isMembers() {
		return members;
	}
	public void setMembers(boolean members) {
		this.members = members;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}
