package com.oopscraft.gealerts.core;

import java.util.Map;

public class Item {
	public String iconURL;
	public String lgIconURL;
	public Long id;
	public String type;
	public String typeIconURL;
	public String name;
	public String description;
	public Map<String,String>  current;
	public Map<String,String>  today;
	public Map<String,String>  day30;
	public Map<String,String>  day90;
	public Map<String,String>  day180;
	public boolean members;
	
	public Item() {
		
	}
	
}
